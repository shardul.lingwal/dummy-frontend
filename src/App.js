/* src/App.js */
import React, { useEffect, useState } from 'react'
import Amplify, { API, graphqlOperation } from 'aws-amplify'
import { createTodo } from './graphql/mutations'
import { listTodos } from './graphql/queries'
import { withAuthenticator } from '@aws-amplify/ui-react'

import awsExports from "./aws-exports";
Amplify.configure(awsExports);

const initialState = { username: '', password: '' }

const App = () => {
  const [formState, setFormState] = useState(initialState)
  const [todos, setTodos] = useState([])

  useEffect(() => {
    fetchTodos()
  }, [])

  function setInput(key, value) {
    setFormState({ ...formState, [key]: value })
  }

  async function fetchTodos() {
    try {
      const todoData = await API.graphql(graphqlOperation(listTodos))
      const todos = todoData.data.listTodos.items
      setTodos(todos)
    } catch (err) { console.log('error fetching todos') }
  }

  async function register() {
    try {
      if (!formState.username || !formState.password) return
      const todo = { ...formState }
      setTodos([...todos, todo])
      setFormState(initialState)
      await API.graphql(graphqlOperation(createTodo, {input: todo}))
    } catch (err) {
      console.log('error creating todo:', err)
    }
  }

  async function dashboard() {
    console.log('getting dashboard');
  }

  return (
    <div>
      <button style={styles.button} onClick={dashboard}>Show All Users</button>
      <div style={styles.container}>
        <h2>Amplify Todos</h2>
        <input
          onChange={event => setInput('username', event.target.value)}
          style={styles.input}
          value={formState.username}
          placeholder="Username"
        />
        <input
          onChange={event => setInput('password', event.target.value)}
          style={styles.input}
          value={formState.password}
          placeholder="Password"
        />
        <button style={styles.button} onClick={register}>Register</button>
        {
          todos.map((todo, index) => (
            <div key={todo.id ? todo.id : index} style={styles.todo}>
              <p style={styles.todoUsername}>{todo.username}</p>
              <p style={styles.todoPassword}>{todo.password}</p>
            </div>
          ))
        }
      </div>
      <div style={styles.dashboardContainer}></div>
    </div>
  )
}

const styles = {
  container: { width: 400, margin: '0 auto', display: 'flex', flexDirection: 'column', justifyContent: 'center', padding: 20 },
  todo: {  marginBottom: 15 },
  input: { border: 'none', backgroundColor: '#ddd', marginBottom: 10, padding: 8, fontSize: 18 },
  todoUsername: { fontSize: 20, fontWeight: 'bold' },
  todoPassword: { marginBottom: 0 },
  button: { backgroundColor: 'black', color: 'white', outline: 'none', fontSize: 18, padding: '12px 0px' },
  dashboardContainer: { width: 700, margin: '0 auto', display: 'flex', flexDirection: 'column', justifyContent: 'center', padding: 20 },
}

// export default withAuthenticator(App)
export default App